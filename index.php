<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
	<link rel="stylesheet" href="./css/bootstrap.min.css">
    <script src="./js/jquery-3.4.1.min.js"></script>
    <script src="./js/popper.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
</head>
 
<body>
	<div class="container">
		<div class="row justify-content-center">
			<h3>CRUD: Create, Read, Update and Delete</h3>
		</div>
		<div class="row justify-content-center">
            <h3>PHP & MySQL</h3>
		</div>
		<div class="row">
			<p>
				<a href="create.php" class="btn btn-success">Create</a>
			</p>
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Name</th>
						<th>Address</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						include_once('connection.php'); 
						$results = mysqli_query($db, "SELECT * FROM contacts");
						while ($row = mysqli_fetch_array($results)) {
							echo '<td>'. $row['name'] . '</td>';
							echo '<td>'. $row['address'] . '</td>';
							echo '<td width=250>';
							echo '<a class="btn btn-info" href="read.php?id='.$row['id'].'">Read</a>';
							echo ' ';
							echo '<a class="btn btn-success" href="update.php?id='.$row['id'].'">Update</a>';
							echo ' ';
							echo '<a class="btn btn-danger" href="delete.php?id='.$row['id'].'">Delete</a>';
							echo '</td>';
							echo '</tr>';
						}
					?>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>
        